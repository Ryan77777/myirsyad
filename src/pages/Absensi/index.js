import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import Svg, {Circle, G} from 'react-native-svg';
import {FloatingButton, Gap, Header, ListAbsensi} from '../../components';

const Absensi = () => {
  const percentage = 4;
  const radius = 40;
  const strokeWidth = 15;
  const max = 5;

  const circleRef = React.useRef();
  const halfCircle = radius + strokeWidth;
  const circumference = 2 * Math.PI * radius;

  const maxPerc = (100 * percentage) / max;
  const strokeDashoffset = circumference - (circumference * maxPerc) / 100;

  return (
    <View style={styles.page}>
      <Header text="Absensi Sekolah" />
      <Gap height={24} />
      <View style={styles.container}>
        {/* Bulan ini */}
        <View style={styles.cardWrapper}>
          <Text style={styles.text}>Terlambat bulan ini</Text>
          <Gap height={15} />
          <View style={{width: radius * 2, height: radius * 2}}>
            <Svg
              height={radius * 2}
              width={radius * 2}
              viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}>
              <G rotation="-90" origin={`${halfCircle}, ${halfCircle}`}>
                <Circle
                  cx="50%"
                  cy="50%"
                  stroke="#FFFFFF"
                  strokeWidth={strokeWidth}
                  r={radius}
                  fill="transparent"
                />
                <Circle
                  ref={circleRef}
                  cx="50%"
                  cy="50%"
                  stroke="#FF9F1A"
                  strokeWidth={strokeWidth}
                  r={radius}
                  fill="transparent"
                  strokeDasharray={circumference}
                  strokeDashoffset={strokeDashoffset}
                  strokeLinecap="round"
                />
              </G>
            </Svg>
            <View style={styles.amountWrapper}>
              <Text style={styles.textAmount}>
                {percentage}/{max}
              </Text>
            </View>
          </View>
        </View>
        <Gap height={10} />
        {/* Tahun ini */}
        <View style={styles.cardContainer}>
          <Text style={styles.text}>Terlambat tahun ini</Text>
          <Gap height={15} />
          <View style={{width: radius * 2, height: radius * 2}}>
            <Svg
              height={radius * 2}
              width={radius * 2}
              viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}>
              <G rotation="-90" origin={`${halfCircle}, ${halfCircle}`}>
                <Circle
                  cx="50%"
                  cy="50%"
                  stroke="#FFFFFF"
                  strokeWidth={strokeWidth}
                  r={radius}
                  fill="transparent"
                />
                <Circle
                  ref={circleRef}
                  cx="50%"
                  cy="50%"
                  stroke="#7D5FFF"
                  strokeWidth={strokeWidth}
                  r={radius}
                  fill="transparent"
                  strokeDasharray={circumference}
                  strokeDashoffset={strokeDashoffset}
                  strokeLinecap="round"
                />
              </G>
            </Svg>
            <View style={styles.amountWrapper}>
              <Text style={styles.textAmount}>
                {percentage}/{max}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <Gap height={24} />
      <View style={styles.line} />
      <Gap height={8} />
      <View style={styles.content}>
        <ListAbsensi title="Hadir" date="Rab, 7 Apr 2021" time="07.00-09.00" />
        <ListAbsensi title="Izin" date="Rab, 7 Apr 2021" time="07.00-09.00" />
        <ListAbsensi title="Sakit" date="Rab, 7 Apr 2021" time="07.00-09.00" />
        <ListAbsensi title="Alpa" date="Rab, 7 Apr 2021" time="07.00-09.00" />
      </View>
      <FloatingButton style={{bottom: normalize(30), right: normalize(15)}} />
    </View>
  );
};

export default Absensi;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  container: {
    flexDirection: 'row',
  },
  cardWrapper: {
    flex: 1,
    backgroundColor: '#CD84F1',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: normalize(21),
    paddingHorizontal: normalize(40),
    paddingVertical: normalize(15),
    borderRadius: normalize(10),
  },
  cardContainer: {
    flex: 1,
    backgroundColor: '#FF9F1A',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: normalize(21),
    paddingHorizontal: normalize(40),
    paddingVertical: normalize(15),
    borderRadius: normalize(10),
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  amountWrapper: {
    backgroundColor: 'transparent',
    width: normalize(60),
    height: normalize(60),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: normalize(8),
    left: normalize(8),
  },
  textAmount: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#FFFFFF',
  },
  content: {
    marginHorizontal: normalize(15),
  },
  line: {
    borderBottomWidth: 1,
    marginHorizontal: normalize(35),
    borderBottomColor: '#D7D7D7',
  },
});
