import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import normalize from 'react-native-normalize';
import {User} from '../../assets';
import {Button, Gap, Header, Input} from '../../components';

const EditProfile = () => {
  const [openProv, setOpenProv] = React.useState(false);
  const [openCity, setOpenCity] = React.useState(false);
  const [value, setValue] = React.useState(null);
  const [prov, setProv] = React.useState([
    {label: 'Jawa Barat', value: 'Jawa Barat'},
    {label: 'Jakarta', value: 'Jakarta'},
  ]);
  const [city, setCity] = React.useState([
    {label: 'Majalengka', value: 'Majalengka'},
    {label: 'Cirebon', value: 'Cirebon'},
  ]);
  return (
    <SafeAreaView style={styles.page}>
      <Header text="Edit Profil" />
      <ScrollView>
        <View style={styles.profileWrapper}>
          <Image source={User} style={styles.profile} />
          <Gap width={16} />
          <View style={styles.borderButton}>
            <Text style={styles.textEditPhoto}>Edit Foto</Text>
          </View>
        </View>
        <Gap height={6} />
        <View style={styles.container}>
          <Input value="Ibnu Soffyan Tsauri" />
          <Input value="ibnusoffyan@gmail.com" />
          <Input value="081999140599" />
          <DropDownPicker
            open={openProv}
            value={'Jawa Barat'}
            items={prov}
            setOpen={setOpenProv}
            setValue={setValue}
            setItems={setProv}
            style={styles.picker}
            placeholder="Provinsi"
            textStyle={styles.textPicker}
            placeholderStyle={styles.placeholderStyle}
            dropDownContainerStyle={styles.dropDownContainerStyle}
            listMode="SCROLLVIEW"
          />
          <DropDownPicker
            open={openCity}
            value={'Cirebon'}
            items={city}
            setOpen={setOpenCity}
            setValue={setValue}
            setItems={setCity}
            style={styles.picker}
            placeholder="Kabupaten"
            textStyle={styles.textPicker}
            placeholderStyle={styles.placeholderStyle}
            dropDownContainerStyle={styles.dropDownContainerStyle}
            listMode="SCROLLVIEW"
          />
          <Input value="Blok Gondangsari, Desa kertasari, Kecamatan Weru, Kabupaten Cirebon" />
        </View>
        <Gap height={24} />
        <View style={styles.button}>
          <Button text="Ubah Profil" />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  profileWrapper: {
    flexDirection: 'row',
    padding: normalize(24),
    alignItems: 'center',
  },
  profile: {
    width: normalize(77),
    height: normalize(77),
    borderRadius: normalize(77),
  },
  borderButton: {
    backgroundColor: '#10AC84',
    paddingHorizontal: normalize(17),
    paddingVertical: normalize(6),
    borderRadius: normalize(20),
  },
  textEditPhoto: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  container: {
    paddingHorizontal: normalize(24),
  },
  picker: {
    backgroundColor: '#F7F6FB',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#D7D7D7',
    borderRadius: 0,
    zIndex: 0,
    marginLeft: normalize(-5),
  },
  textPicker: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
  placeholderStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#C4C4C4',
    marginLeft: normalize(-5),
  },
  dropDownContainerStyle: {
    backgroundColor: '#FFFFFF',
    borderColor: '#C4C4C4',
    zIndex: 1,
  },
  button: {
    paddingHorizontal: normalize(34),
  },
});
