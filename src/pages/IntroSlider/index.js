import React, {useRef, useState} from 'react';
import {
  Animated,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import Sliders from '../../assets/JSON/IntroSlider';
import {
  Button,
  Gap,
  IntroSliderItem,
  IntroSliderPagination,
} from '../../components';

const IntroSlider = ({navigation}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const slidersRef = useRef(null);

  const viewableItemsChanged = useRef(({viewableItems}) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({viewAreaCoveragePercentThreshold: 50}).current;

  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <FlatList
          data={Sliders}
          renderItem={({item}) => <IntroSliderItem item={item} />}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          bounces={false}
          keyExtractor={item => item.id}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false},
          )}
          onViewableItemsChanged={viewableItemsChanged}
          viewabilityConfig={viewConfig}
          ref={slidersRef}
        />
      </View>
      {currentIndex !== 2 ? <Gap height={92} /> : <Gap height={17} />}
      <IntroSliderPagination data={Sliders} scrollX={scrollX} />
      {currentIndex === 2 && (
        <View style={styles.footer}>
          <Button
            text="Buat Akun"
            onPress={() => navigation.replace('Register')}
          />
          <Gap height={24} />
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => navigation.replace('Login')}>
            <Text style={styles.text}>Masuk Sekarang</Text>
          </TouchableOpacity>
        </View>
      )}
      <Gap height={55} />
    </View>
  );
};

export default IntroSlider;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 3,
  },
  footer: {
    width: '100%',
    paddingHorizontal: normalize(36),
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#1B6007',
    textAlign: 'center',
  },
});
