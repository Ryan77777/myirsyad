import _ from 'lodash';
import React, {Component, useCallback} from 'react';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  AgendaList,
  CalendarProvider,
  ExpandableCalendar,
  WeekCalendar,
} from 'react-native-calendars';
import normalize from 'react-native-normalize';
import {Gap, Header} from '../../components';

const today = new Date().toISOString().split('T')[0];
const fastDate = getPastDate(3);
const futureDates = getFutureDates(9);
const dates = [fastDate, today].concat(futureDates);

function getFutureDates(days) {
  const array = [];
  for (let index = 1; index <= days; index++) {
    const date = new Date(Date.now() + 864e5 * index); // 864e5 == 86400000 == 24*60*60*1000
    const dateString = date.toISOString().split('T')[0];
    array.push(dateString);
  }
  return array;
}

function getPastDate(days) {
  return new Date(Date.now() - 864e5 * days).toISOString().split('T')[0];
}

const ITEMS = [
  {
    title: dates[0],
    data: [{hour: '07.00 - 09.00', title: 'UTS Matematika'}],
  },
  {
    title: dates[1],
    data: [
      {hour: '07.00 - 09.00', title: 'UTS IPA'},
      {hour: '10.00 - 12.00', title: 'UTS Fisika'},
    ],
  },
  {
    title: dates[3],
    data: [{hour: '10.00 - 12.00', title: 'UTS Matematika'}],
  },
  {title: dates[4], data: [{}]},
  {
    title: dates[6],
    data: [{hour: '10.00 - 12.00', title: 'UTS Matematika'}],
  },
  {title: dates[7], data: [{}]},
];

function getMarkedDates(items) {
  const marked = {};
  items.forEach(item => {
    if (item.data && item.data.length > 0 && !_.isEmpty(item.data[0])) {
      marked[item.title] = {marked: true};
    } else {
      marked[item.title] = {disabled: true};
    }
  });
  console.log(marked);
  return marked;
}

export default class ExpandableCalendarScreen extends Component {
  marked = getMarkedDates(ITEMS);

  onDateChanged = (/* date, updateSource */) => {
    // console.warn('ExpandableCalendarScreen onDateChanged: ', date, updateSource);
    // fetch and set data for date + week ahead
  };

  onMonthChange = (/* month, updateSource */) => {
    // console.warn('ExpandableCalendarScreen onMonthChange: ', month, updateSource);
  };

  renderItem = ({item}) => {
    return <AgendaItem item={item} />;
  };

  render() {
    return (
      <>
        <Header text="Jadwal Ujian" />
        <CalendarProvider
          date={ITEMS[0].title}
          onDateChanged={this.onDateChanged}
          onMonthChange={this.onMonthChange}
          disabledOpacity={0.7}>
          {this.props.weekView ? (
            <WeekCalendar
              testID={'weekCalendar'}
              firstDay={1}
              markedDates={this.marked}
            />
          ) : (
            <ExpandableCalendar
              testID={'expandableCalendar'}
              firstDay={1}
              markedDates={this.marked}
              animateScroll
            />
          )}
          <AgendaList sections={ITEMS} renderItem={this.renderItem} />
        </CalendarProvider>
      </>
    );
  }
}

const AgendaItem = React.memo(function AgendaItem(props) {
  const {item} = props;

  const itemPressed = useCallback(() => {
    Alert.alert(item.title);
  }, []);

  if (_.isEmpty(item)) {
    return (
      <View style={styles.emptyItem}>
        <Text style={styles.emptyItemText}>No Events Planned Today</Text>
      </View>
    );
  }

  return (
    <TouchableOpacity onPress={itemPressed} style={styles.item} testID={'item'}>
      <Text style={styles.itemTitleText}>{item.title}</Text>
      <Gap height={6} />
      <Text style={styles.itemHourText}>{item.hour}</Text>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FFFFFF',
    paddingVertical: normalize(18),
    paddingHorizontal: normalize(12),
    marginHorizontal: normalize(15),
    marginVertical: normalize(16),
    borderRadius: normalize(10),
  },
  itemTitleText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#000000',
  },
  itemHourText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
  },
  emptyItem: {
    height: normalize(50),
    paddingLeft: normalize(20),
    justifyContent: 'center',
  },
  emptyItemText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#8B8B8B',
  },
});
