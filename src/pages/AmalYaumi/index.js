import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import normalize from 'react-native-normalize';
import {IcPlus} from '../../assets';
import {Gap, Header, ListData, SlidingUpPanel} from '../../components';

const AmalYaumi = () => {
  const [isShow, setIsShow] = React.useState(false);
  const close = () => {
    setIsShow(false);
  };
  return (
    <View style={styles.page}>
      <Header text="Amal Yaumi" />
      <Gap height={10} />
      <ListData title="Sholat Dhuha" date="17 Apr 2021" />
      <ListData
        title="Menghafal Al-quran"
        date="17 Apr 2021"
        detail="(Qs. Al-baqarah Ayat 1-20)"
      />
      <ListData title="Puasa sunnah" date="17 Apr 2021" />
      <ListData title="Membaca Al-quran" date="17 Apr 2021" />

      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.button}
        onPress={() => setIsShow(!isShow)}>
        <IcPlus />
      </TouchableOpacity>

      <SlidingUpPanel
        title="Tambah Aktifitas"
        show={isShow}
        closePopup={close}
        type="TambahAmal"
      />
    </View>
  );
};

export default AmalYaumi;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  button: {
    position: 'absolute',
    width: normalize(60),
    height: normalize(60),
    borderRadius: normalize(60 / 2),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF9F1A',
    elevation: 5,
    right: normalize(15),
    bottom: normalize(30),
  },
});
