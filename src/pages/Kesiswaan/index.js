import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {
  BackgroundHeader,
  BackgroundHome,
  IcArrowDown,
  IcEMoney,
  IcInternet,
  IcMore,
  IcNotification,
  IcPDAM,
  IcPLN,
  IcPulsa,
  IcRouter,
  IcTelkom,
  LogoWhite,
  User,
} from '../../assets';
import {Gap, ListNews} from '../../components';

const Kesiswaan = ({navigation}) => {
  return (
    <View style={styles.page}>
      {/* Header */}
      <View style={styles.header}>
        <ImageBackground
          source={BackgroundHeader}
          style={styles.backgroundHeader}>
          <LogoWhite />
          <View>
            <View style={styles.notificationAmount}>
              <Text style={styles.textNotif}>1</Text>
            </View>
            <Image source={IcNotification} style={styles.notification} />
          </View>
        </ImageBackground>
      </View>
      <ScrollView>
        <Image source={BackgroundHome} style={styles.background} />
        {/* Card Profile */}
        <View style={styles.cardProfile}>
          <Image source={User} style={styles.profile} />
          <View style={styles.userContainer}>
            <Text style={styles.name}>Yudhi Pamana</Text>
            <Text style={styles.class}>Kelas XII - Ipa-1</Text>
            <Text style={styles.address}>Cirebon, Jawa Barat</Text>
          </View>
        </View>
        {/* Main Menu */}
        <View style={styles.cardMainMenu}>
          <View style={styles.row}>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('JadwalPelajaran')}>
              <IcPulsa />
              <Text style={styles.textMain}>Jadwal Pelajaran</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('JadwalUjian')}>
              <IcInternet />
              <Text style={styles.textMain}>Jadwal Ujian</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('NilaiUjian')}>
              <IcPLN />
              <Text style={styles.textMain}>Nilai Ujian</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('Absensi')}>
              <IcEMoney />
              <Text style={styles.textMain}>Absensi</Text>
            </TouchableOpacity>
          </View>
          <Gap height={23} />
          <View style={styles.row}>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('MateriPelajaran')}>
              <IcTelkom />
              <Text style={styles.textMain}>Materi Pelajaran</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('AmalYaumi')}>
              <IcPDAM />
              <Text style={styles.textMain}>Amal Yaumi</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.mainMenu}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('PrestasiSiswa')}>
              <IcRouter />
              <Text style={styles.textMain}>Prestasi Siswa</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcMore />
              <Text style={styles.textMain}>Lainnya</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* News */}
        <View style={styles.newsContainer}>
          <View style={styles.newsHeader}>
            <Text style={styles.heading}>Berita MyIrsyad</Text>
            <TouchableOpacity style={styles.more}>
              <Text style={styles.textMore}>Selengkapnya</Text>
              <Gap width={8} />
              <IcArrowDown />
            </TouchableOpacity>
          </View>
          <ListNews />
          <ListNews />
        </View>
      </ScrollView>
    </View>
  );
};

export default Kesiswaan;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  header: {},
  backgroundHeader: {
    flexDirection: 'row',
    height: normalize(98),
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    zIndex: 1,
  },
  background: {
    width: '100%',
    height: normalize(200),
    marginTop: normalize(-86),
  },
  notification: {
    width: normalize(24),
    height: normalize(24),
  },
  notificationAmount: {
    width: normalize(15),
    height: normalize(15),
    borderRadius: normalize(15),
    backgroundColor: '#E74C3C',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -3,
    right: 0,
    zIndex: 1,
  },
  textNotif: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  cardProfile: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: normalize(30),
    marginTop: normalize(-55),
    borderRadius: normalize(10),
    elevation: 2,
    padding: normalize(15),
    flexDirection: 'row',
  },
  profile: {
    width: normalize(72),
    height: normalize(72),
    borderRadius: normalize(10),
  },
  userContainer: {
    marginLeft: normalize(16),
    justifyContent: 'center',
  },
  name: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#000000',
  },
  class: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(10),
    color: '#8B8B8B',
  },
  address: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(10),
    color: '#8B8B8B',
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
  },
  spacing: {
    width: 1,
    height: normalize(30),
    backgroundColor: '#3D3D3D',
  },
  cardMainMenu: {
    backgroundColor: '#FFFFFF',
    borderRadius: normalize(10),
    marginTop: normalize(24),
    marginBottom: normalize(10),
    marginHorizontal: normalize(15),
    paddingHorizontal: normalize(10),
    paddingVertical: normalize(16),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  mainMenu: {
    width: normalize(55),
    height: normalize(80),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textMain: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
    textAlign: 'center',
  },
  newsContainer: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: normalize(15),
    paddingTop: normalize(16),
  },
  newsHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: normalize(24),
  },
  heading: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: '#3D3D3D',
  },
  more: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textMore: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#4584FF',
  },
});
