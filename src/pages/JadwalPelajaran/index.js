import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import data from '../../assets/JSON/JadwalPelajaran';
import {Transition, Transitioning} from 'react-native-reanimated';
import {Gap, Header} from '../../components';
import normalize from 'react-native-normalize';
import {IcArrowDownDark, IcArrowUpDark, IcCalendar} from '../../assets';

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={200} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={200} />
  </Transition.Together>
);

export default function JadwalPelajaran() {
  const [currentIndex, setCurrentIndex] = React.useState(null);
  const ref = React.useRef();

  return (
    <View removeClippedSubviews style={styles.container}>
      <Header text="Jadwal Pelajaran" />
      <Gap height={28} />
      <Transitioning.View
        ref={ref}
        transition={transition}
        style={styles.container}>
        {data.map(({day, subjects}, index) => {
          return (
            <TouchableOpacity
              key={day}
              onPress={() => {
                ref.current.animateNextTransition();
                setCurrentIndex(index === currentIndex ? null : index);
              }}
              activeOpacity={0.9}>
              <View style={[styles.card]}>
                <View style={styles.cardHeader}>
                  <View style={styles.leftCard}>
                    <Image source={IcCalendar} />
                    <Gap width={16} />
                    <Text style={[styles.heading]}>{day}</Text>
                  </View>
                  {index === currentIndex ? (
                    <IcArrowUpDark />
                  ) : (
                    <IcArrowDownDark />
                  )}
                </View>
                {index === currentIndex && (
                  <View style={styles.subjectList}>
                    {subjects.map(subject => {
                      return (
                        <View
                          key={subject.subject}
                          style={[
                            styles.scheduleContainer,
                            {backgroundColor: subject.bg},
                          ]}>
                          <Text style={styles.time}>{subject.time}</Text>
                          <View style={styles.line} />
                          <Gap width={16} />
                          <View>
                            <Text style={styles.subject}>
                              {subject.subject}
                            </Text>
                            <Text style={styles.class}>
                              Kelas {subject.class}
                            </Text>
                          </View>
                        </View>
                      );
                    })}
                  </View>
                )}
              </View>
            </TouchableOpacity>
          );
        })}
      </Transitioning.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  card: {
    paddingLeft: normalize(8),
    paddingRight: normalize(44 - 15),
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: '#8B8B8B',
    marginHorizontal: normalize(15),
  },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: normalize(16),
  },
  leftCard: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  heading: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#3D3D3D',
  },
  subjectList: {
    marginTop: normalize(5),
    marginBottom: normalize(4),
  },
  scheduleContainer: {
    width: '110.5%',
    marginLeft: normalize(-5),
    paddingHorizontal: normalize(8),
    paddingVertical: normalize(6),
    borderRadius: normalize(7),
    marginBottom: normalize(12),
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    borderWidth: 1,
    height: normalize(34),
    borderColor: '#FFFFFF',
  },
  time: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#FFFFFF',
    width: normalize(75),
  },
  subject: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  class: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
});
