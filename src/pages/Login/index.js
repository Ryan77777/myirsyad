import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {
  IcCheck,
  IcHide,
  IcLock,
  IcShow,
  ILLogin,
  Indonesia,
} from '../../assets';
import {Button, Gap} from '../../components';

const Login = ({navigation}) => {
  const [isHidden, setIsHidden] = React.useState(true);
  return (
    <View style={styles.page}>
      <ScrollView>
        <View>
          <Gap height={60} />
          <View style={styles.illustration}>
            <ILLogin />
          </View>
          <Gap height={20} />
          <Text style={styles.text}>Masuk</Text>
          <Gap height={20} />
        </View>
        <View style={styles.card}>
          <Gap height={24} />
          <View style={styles.inputPhoneNumber}>
            <Indonesia />
            <Gap width={16} />
            <Text style={styles.codeNumber}>(+62)</Text>
            <Gap width={10} />
            <TextInput
              placeholder="8x xxx xxx xxx"
              style={styles.input}
              placeholderTextColor="#C4C4C4"
            />
            <Gap width={10} />
            {/* Icon Jika nomor sudah terdaftar */}
            <IcCheck />
          </View>
          <Gap height={10} />
          <View style={styles.inputPhoneNumber}>
            <IcLock />
            <Gap width={10} />
            <TextInput
              placeholder="Masukkan Password"
              style={styles.input}
              placeholderTextColor="#C4C4C4"
              secureTextEntry={isHidden}
            />
            <TouchableOpacity
              style={styles.hide}
              onPress={() => setIsHidden(!isHidden)}>
              {isHidden ? (
                <Image source={IcShow} style={styles.icon} />
              ) : (
                <Image source={IcHide} style={styles.icon} />
              )}
            </TouchableOpacity>
          </View>
          <Gap height={10} />
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => navigation.navigate('ForgotPassword')}>
            <Text style={styles.forgotPassword}>Lupa Password?</Text>
          </TouchableOpacity>
          <Gap height={13} />
          <View style={styles.button}>
            <Button
              text="Masuk"
              onPress={() => navigation.navigate('VerifikasiOTP')}
            />
          </View>
          <Gap height={16} />
        </View>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.footer}
          onPress={() => navigation.navigate('Register')}>
          <Text style={styles.textFooter}>Anda tidak mempunyai akun?</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  illustration: {
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(20),
    color: '#1E272E',
    textAlign: 'center',
  },
  card: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: normalize(15),
    borderRadius: normalize(20),
    paddingHorizontal: normalize(16),
  },
  inputPhoneNumber: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: normalize(10),
    paddingLeft: normalize(24),
    paddingHorizontal: normalize(16),
  },
  input: {
    width: '60%',
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(15),
    color: '#3D3D3D',
  },
  codeNumber: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(15),
    color: '#3D3D3D',
  },
  hide: {
    position: 'absolute',
    right: normalize(20),
  },
  icon: {
    width: normalize(24),
    height: normalize(24),
  },
  forgotPassword: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#4584FF',
    textAlign: 'right',
  },
  button: {
    paddingHorizontal: normalize(19),
  },
  footer: {
    marginTop: normalize(64),
    alignItems: 'center',
  },
  textFooter: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#4584FF',
  },
});
