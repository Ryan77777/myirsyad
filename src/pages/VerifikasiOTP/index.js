import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {ILLogin} from '../../assets';
import {Button, Gap} from '../../components';

const VerifikasiOTP = ({navigation}) => {
  return (
    <View style={styles.page}>
      <ScrollView>
        <View>
          <Gap height={60} />
          <View style={styles.illustration}>
            <ILLogin />
          </View>
          <Gap height={20} />
          <Text style={styles.text}>Verifikasi</Text>
          <Gap height={9} />
          <View style={{alignItems: 'center'}}>
            <Text style={styles.desc}>
              Masukan 4 kode digit yang terkirim ke +6281151645875
            </Text>
          </View>
          <Gap height={16} />
        </View>
        <View style={styles.card}>
          <Gap height={62} />
          <View style={styles.form}>
            <View style={styles.formContainer}>
              <TextInput style={styles.input} />
            </View>
            <View style={styles.formContainer}>
              <TextInput style={styles.input} />
            </View>
            <View style={styles.formContainer}>
              <TextInput style={styles.input} />
            </View>
            <View style={styles.formContainer}>
              <TextInput style={styles.input} />
            </View>
          </View>
          <Gap height={32} />
          <View style={styles.button}>
            <Button
              text="Masuk"
              onPress={() => navigation.replace('MainApp')}
            />
          </View>
          <Gap height={16} />
        </View>
        <View style={styles.footer}>
          <Text style={styles.textFooter}>Tidak Menerimanya? </Text>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => navigation.navigate('Register')}>
            <Text style={styles.textFooterBold}>Kirim Lagi</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default VerifikasiOTP;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  illustration: {
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(20),
    color: '#1E272E',
    textAlign: 'center',
  },
  card: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: normalize(15),
    borderRadius: normalize(20),
    paddingHorizontal: normalize(16),
  },
  form: {
    flexDirection: 'row',
    paddingHorizontal: normalize(40),
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: normalize(36.75),
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(20),
    color: '#3D3D3D',
    borderBottomWidth: 1,
    borderBottomColor: '#C4C4C4',
    paddingLeft: normalize(12),
  },
  button: {
    paddingHorizontal: normalize(19),
  },
  footer: {
    flexDirection: 'row',
    marginTop: normalize(16),
    alignItems: 'center',
    justifyContent: 'center',
  },
  textFooter: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#4584FF',
  },
  textFooterBold: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(12),
    color: '#4584FF',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#1E272E',
    textAlign: 'center',
    width: normalize(230),
  },
});
