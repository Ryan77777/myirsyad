import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import Svg, {Circle, G} from 'react-native-svg';
import {Gap, Header, ListUjian} from '../../components';

const NilaiUjian = () => {
  const percentage = 71.5;
  const radius = 40;
  const strokeWidth = 15;
  const max = 100;

  const circleRef = React.useRef();
  const halfCircle = radius + strokeWidth;
  const circumference = 2 * Math.PI * radius;

  const maxPerc = (100 * percentage) / max;
  const strokeDashoffset = circumference - (circumference * maxPerc) / 100;

  return (
    <View style={styles.page}>
      <Header text="Nilai Ujian" />
      <Gap height={24} />
      {/* Semester ini */}
      <View style={styles.cardWrapper}>
        <Text style={styles.text}>Nilai rata-rata semester ini</Text>
        <View style={{width: radius * 2, height: radius * 2}}>
          <Svg
            height={radius * 2}
            width={radius * 2}
            viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}>
            <G rotation="-90" origin={`${halfCircle}, ${halfCircle}`}>
              <Circle
                cx="50%"
                cy="50%"
                stroke="#FFFFFF"
                strokeWidth={strokeWidth}
                r={radius}
                fill="transparent"
              />
              <Circle
                ref={circleRef}
                cx="50%"
                cy="50%"
                stroke="#FF9F1A"
                strokeWidth={strokeWidth}
                r={radius}
                fill="transparent"
                strokeDasharray={circumference}
                strokeDashoffset={strokeDashoffset}
                strokeLinecap="round"
              />
            </G>
          </Svg>
          <View style={styles.amountWrapper}>
            <Text style={styles.textAmount}>{percentage}</Text>
          </View>
        </View>
      </View>
      <Gap height={10} />
      {/* Tahun ini */}
      <View style={styles.cardContainer}>
        <Text style={styles.text}>Nilai rata-rata tahun ini</Text>
        <View style={{width: radius * 2, height: radius * 2}}>
          <Svg
            height={radius * 2}
            width={radius * 2}
            viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}>
            <G rotation="-90" origin={`${halfCircle}, ${halfCircle}`}>
              <Circle
                cx="50%"
                cy="50%"
                stroke="#FFFFFF"
                strokeWidth={strokeWidth}
                r={radius}
                fill="transparent"
              />
              <Circle
                ref={circleRef}
                cx="50%"
                cy="50%"
                stroke="#7D5FFF"
                strokeWidth={strokeWidth}
                r={radius}
                fill="transparent"
                strokeDasharray={circumference}
                strokeDashoffset={strokeDashoffset}
                strokeLinecap="round"
              />
            </G>
          </Svg>
          <View style={styles.amountWrapper}>
            <Text style={styles.textAmount}>{percentage}</Text>
          </View>
        </View>
      </View>
      <Gap height={24} />
      <View style={styles.line} />
      <Gap height={24} />
      <View style={styles.content}>
        <ListUjian
          title="UTS Matematika"
          date="Rab, 7 Apr 2021"
          time="07.00-09.00"
          value={87.5}
        />
        <ListUjian
          title="Ulangan Bhs. Inggis"
          date="Rab, 7 Apr 2021"
          time="07.00-09.00"
          value={95.5}
        />
        <ListUjian
          title="UTS Fisika"
          date="Rab, 7 Apr 2021"
          time="07.00-09.00"
          value={55.5}
        />
      </View>
    </View>
  );
};

export default NilaiUjian;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  cardWrapper: {
    flexDirection: 'row',
    backgroundColor: '#CD84F1',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: normalize(15),
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(6),
    borderRadius: normalize(10),
  },
  cardContainer: {
    flexDirection: 'row',
    backgroundColor: '#FF9F1A',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: normalize(15),
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(6),
    borderRadius: normalize(10),
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  amountWrapper: {
    backgroundColor: 'transparent',
    width: normalize(60),
    height: normalize(60),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: normalize(8),
    left: normalize(8),
  },
  textAmount: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  content: {
    marginHorizontal: normalize(15),
  },
  line: {
    borderBottomWidth: 1,
    marginHorizontal: normalize(35),
    borderBottomColor: '#D7D7D7',
  },
});
