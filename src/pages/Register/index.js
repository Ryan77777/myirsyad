import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {IcHide, IcShow, LogoDark} from '../../assets';
import {Button, Gap, Input} from '../../components';
import DropDownPicker from 'react-native-dropdown-picker';

const Register = ({navigation}) => {
  const [isHidden, setIsHidden] = React.useState(true);
  const [openProv, setOpenProv] = React.useState(false);
  const [openCity, setOpenCity] = React.useState(false);
  const [value, setValue] = React.useState(null);
  const [prov, setProv] = React.useState([
    {label: 'Jawa Barat', value: 'Jawa Barat'},
    {label: 'Jakarta', value: 'Jakarta'},
  ]);
  const [city, setCity] = React.useState([
    {label: 'Majalengka', value: 'Majalengka'},
    {label: 'Cirebon', value: 'Cirebon'},
  ]);
  return (
    <View style={styles.page}>
      <ScrollView>
        <Gap height={30} />
        <View style={styles.logo}>
          <LogoDark />
        </View>
        <Gap height={20} />
        <View style={styles.card}>
          <Text style={styles.text}>Daftar Akun</Text>
          <Input placeholder="Nama" />
          <Input placeholder="Email" />
          <Input placeholder="No Hp" />
          <DropDownPicker
            open={openProv}
            value={value}
            items={prov}
            setOpen={setOpenProv}
            setValue={setValue}
            setItems={setProv}
            style={styles.picker}
            placeholder="Provinsi"
            textStyle={styles.textPicker}
            placeholderStyle={styles.placeholderStyle}
            dropDownContainerStyle={styles.dropDownContainerStyle}
            listMode="SCROLLVIEW"
          />
          <DropDownPicker
            open={openCity}
            value={value}
            items={city}
            setOpen={setOpenCity}
            setValue={setValue}
            setItems={setCity}
            style={styles.picker}
            placeholder="Kabupaten"
            textStyle={styles.textPicker}
            placeholderStyle={styles.placeholderStyle}
            dropDownContainerStyle={styles.dropDownContainerStyle}
            listMode="SCROLLVIEW"
          />
          <Input placeholder="Alamat" />
          <View style={styles.container}>
            <TextInput
              placeholder="Password"
              placeholderTextColor="#C4C4C4"
              style={styles.inputPassword}
              secureTextEntry={isHidden}
            />
            <TouchableOpacity
              style={styles.hide}
              onPress={() => setIsHidden(!isHidden)}>
              {isHidden ? (
                <Image source={IcShow} style={styles.icon} />
              ) : (
                <Image source={IcHide} style={styles.icon} />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.container}>
            <TextInput
              placeholder="Konfirmasi Password"
              placeholderTextColor="#C4C4C4"
              style={styles.inputPassword}
              secureTextEntry={isHidden}
            />
            <TouchableOpacity
              style={styles.hide}
              onPress={() => setIsHidden(!isHidden)}>
              {isHidden ? (
                <Image source={IcShow} style={styles.icon} />
              ) : (
                <Image source={IcHide} style={styles.icon} />
              )}
            </TouchableOpacity>
          </View>
          <Gap height={32} />
          <Button
            text="Daftar Sekarang"
            onPress={() => navigation.replace('MainApp')}
          />
        </View>
        <View style={styles.footer}>
          <Text style={styles.textFooter}>Sudah punya Akun?</Text>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.textFooterBold}> Login</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  logo: {
    alignItems: 'center',
  },
  card: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: normalize(15),
    borderRadius: normalize(20),
    paddingHorizontal: normalize(15),
    paddingTop: normalize(20),
    paddingBottom: normalize(24),
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(18),
    color: '#1E272E',
    textAlign: 'center',
  },
  footer: {
    flexDirection: 'row',
    marginTop: normalize(40),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textFooter: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#4584FF',
  },
  textFooterBold: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(12),
    color: '#4584FF',
  },
  container: {
    flexDirection: 'row',
  },
  inputPassword: {
    width: '100%',
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#3D3D3D',
    borderBottomWidth: 1,
    borderBottomColor: '#C4C4C4',
  },
  icon: {
    width: normalize(24),
    height: normalize(24),
    position: 'absolute',
    top: normalize(14),
    right: normalize(20),
  },
  picker: {
    backgroundColor: '#FFFFFF',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#A8A8A8',
    borderRadius: 0,
    zIndex: 0,
    marginLeft: normalize(-5),
  },
  textPicker: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
  placeholderStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#C4C4C4',
    marginLeft: normalize(-5),
  },
  dropDownContainerStyle: {
    backgroundColor: '#FFFFFF',
    borderColor: '#C4C4C4',
    zIndex: 1,
  },
});
