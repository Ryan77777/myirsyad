import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import normalize from 'react-native-normalize';
import {IcArrowLeft, LogoWhite} from '../../assets';
import {Gap, TicketView} from '../../components';

const DetailTransaction = ({navigation}) => {
  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.goBack()}>
          <IcArrowLeft />
        </TouchableOpacity>
        <Gap width={16} />
        <Text style={styles.text}>Detail Transaksi</Text>
      </View>
      <View style={styles.content}>
        <TicketView />
      </View>
      <View style={styles.logo}>
        <LogoWhite />
      </View>
    </View>
  );
};

export default DetailTransaction;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#10AC84',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: normalize(25),
    paddingHorizontal: normalize(15),
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(18),
    color: '#FFFFFF',
  },
  content: {
    marginTop: normalize(20),
  },
  logo: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: normalize(50),
  },
});
