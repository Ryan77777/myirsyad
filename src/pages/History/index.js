import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {Header} from '../../assets';
import normalize from 'react-native-normalize';
import ListHistory from '../../components/molecules/ListHistory';

const History = ({navigation}) => {
  return (
    <View style={styles.page}>
      <ImageBackground source={Header} style={styles.header}>
        <Text style={styles.textHeader}>Riwayat Transaksi</Text>
      </ImageBackground>
      <View style={styles.container}>
        <ListHistory
          provider="Pulsa"
          date="13 Mei 2021"
          time="08:21"
          price={10000}
          onPress={() => navigation.navigate('DetailTransaction')}
        />
        <ListHistory
          provider="Pulsa"
          date="14 Mei 2021"
          time="08:23"
          price={20000}
        />
      </View>
    </View>
  );
};

export default History;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  header: {
    height: normalize(84),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHeader: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(18),
    color: '#FFFFFF',
  },
  container: {
    flex: 1,
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(24),
  },
});
