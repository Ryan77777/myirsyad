import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {
  BackgroundProfile,
  IcDoc,
  IcEdit,
  IcLockBlack,
  IcLogout,
  User,
} from '../../assets';
import {Gap, ListProfile} from '../../components';
import normalize from 'react-native-normalize';

const Profile = ({navigation}) => {
  return (
    <View style={styles.page}>
      <Image source={BackgroundProfile} style={styles.background} />
      <View style={styles.userContainer}>
        <Image source={User} style={styles.user} />
        <Gap height={16} />
        <Text style={styles.name}>Soffyan Tsauri</Text>
        <Gap height={4} />
        <Text style={styles.phone}>082 124 041 124</Text>
      </View>
      <Gap height={46} />
      <View>
        <ListProfile
          text="Edit Profil"
          icon={<IcEdit />}
          onPress={() => navigation.navigate('EditProfile')}
        />
        <ListProfile text="Ubah Password" icon={<IcLockBlack />} />
        <ListProfile text="Kebijakan Privasi" icon={<IcDoc />} />
        <ListProfile text="Aturan Pengguna" icon={<IcDoc />} />
        <ListProfile text="Keluar" icon={<IcLogout />} />
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  background: {
    width: '100%',
    height: normalize(200),
  },
  userContainer: {
    alignItems: 'center',
    marginTop: normalize(-63),
  },
  user: {
    width: normalize(126),
    height: normalize(126),
    borderRadius: normalize(10),
  },
  name: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(20),
    color: '#000000',
  },
  phone: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#000000',
  },
});
