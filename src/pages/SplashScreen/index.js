import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Background, Logo} from '../../assets';

const SplashScreen = ({navigation}) => {
  React.useEffect(() => {
    setTimeout(() => {
      navigation.replace('IntroSlider');
    }, 3000);
  });
  return (
    <View style={styles.page}>
      <View style={styles.logo}>
        <Logo />
      </View>
      <View style={styles.back}>
        <Background />
      </View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#1B6007',
  },
  logo: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  back: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
});
