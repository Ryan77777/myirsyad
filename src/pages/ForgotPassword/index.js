import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {IcCheck, IcHide, IcLock, IcShow, ILLogin} from '../../assets';
import {Button, Gap} from '../../components';

const ForgotPassword = ({navigation}) => {
  const [isHidden, setIsHidden] = React.useState(true);
  return (
    <View style={styles.page}>
      <ScrollView>
        <View>
          <Gap height={60} />
          <View style={styles.illustration}>
            <ILLogin />
          </View>
          <Gap height={20} />
          <Text style={styles.text}>Kata Sandi Baru</Text>
          <Gap height={9} />
          <View style={{alignItems: 'center'}}>
            <Text style={styles.desc}>
              Masukan kode reset kata sandi yang sudah kami kirim ke
              +6281151645875
            </Text>
          </View>
          <Gap height={16} />
        </View>
        <View style={styles.card}>
          <Gap height={24} />
          <View style={styles.inputPhoneNumber}>
            <IcLock />
            <Gap width={10} />
            <TextInput
              placeholder="Kode reset kata sandi"
              style={styles.input}
              placeholderTextColor="#C4C4C4"
            />
            <Gap width={57} />
            {/* Icon Jika nomor sudah terdaftar */}
            <IcCheck />
          </View>
          <Gap height={10} />
          <View style={styles.inputPhoneNumber}>
            <IcLock />
            <Gap width={10} />
            <TextInput
              placeholder="Password Baru"
              style={styles.input}
              placeholderTextColor="#C4C4C4"
              secureTextEntry={isHidden}
            />
            <TouchableOpacity
              style={styles.hide}
              onPress={() => setIsHidden(!isHidden)}>
              {isHidden ? (
                <Image source={IcShow} style={styles.icon} />
              ) : (
                <Image source={IcHide} style={styles.icon} />
              )}
            </TouchableOpacity>
          </View>
          <Gap height={10} />
          <View style={styles.inputPhoneNumber}>
            <IcLock />
            <Gap width={10} />
            <TextInput
              placeholder="Konfirmari Password"
              style={styles.input}
              placeholderTextColor="#C4C4C4"
              secureTextEntry={isHidden}
            />
            <TouchableOpacity
              style={styles.hide}
              onPress={() => setIsHidden(!isHidden)}>
              {isHidden ? (
                <Image source={IcShow} style={styles.icon} />
              ) : (
                <Image source={IcHide} style={styles.icon} />
              )}
            </TouchableOpacity>
          </View>
          <Gap height={16} />
          <View style={styles.button}>
            <Button text="Masuk" />
          </View>
          <Gap height={16} />
        </View>
        <View style={styles.footer}>
          <Text style={styles.textFooter}>Tidak Menerimanya? </Text>
          <TouchableOpacity activeOpacity={0.7}>
            <Text style={styles.textFooterBold}>Kirim Lagi</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  illustration: {
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(20),
    color: '#1E272E',
    textAlign: 'center',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#1E272E',
    textAlign: 'center',
    width: normalize(275),
  },
  card: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: normalize(15),
    borderRadius: normalize(20),
    paddingHorizontal: normalize(16),
  },
  inputPhoneNumber: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: normalize(10),
    paddingLeft: normalize(24),
    paddingHorizontal: normalize(16),
  },
  input: {
    width: '60%',
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(15),
    color: '#3D3D3D',
  },
  codeNumber: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(15),
    color: '#3D3D3D',
  },
  hide: {
    position: 'absolute',
    right: normalize(20),
  },
  icon: {
    width: normalize(24),
    height: normalize(24),
  },
  forgotPassword: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#4584FF',
    textAlign: 'right',
  },
  button: {
    paddingHorizontal: normalize(19),
  },
  footer: {
    flexDirection: 'row',
    marginTop: normalize(8),
    alignItems: 'center',
    justifyContent: 'center',
  },
  textFooter: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#4584FF',
  },
  textFooterBold: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(12),
    color: '#4584FF',
  },
});
