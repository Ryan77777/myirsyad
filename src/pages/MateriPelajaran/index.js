import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Header, SubjectTabs} from '../../components';

const MateriPelajaran = () => {
  return (
    <View style={styles.page}>
      <Header text="Materi Pelajaran" />
      <SubjectTabs />
    </View>
  );
};

export default MateriPelajaran;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
});
