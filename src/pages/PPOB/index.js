import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {
  BackgroundHeader,
  BackgroundHome,
  IcArrowDown,
  IcEMoney,
  IcInternet,
  IcMore,
  IcNotification,
  IcPDAM,
  IcPLN,
  IcPulsa,
  IcRouter,
  IcScan,
  IcTelkom,
  IcTopUp,
  IcTransfer,
  IcWallet,
  LogoWhite,
} from '../../assets';
import {Gap, ListNews} from '../../components';
import Number from '../../components/molecules/Number';

const PPOB = () => {
  return (
    <View style={styles.page}>
      {/* Header */}
      <View style={styles.header}>
        <ImageBackground
          source={BackgroundHeader}
          style={styles.backgroundHeader}>
          <LogoWhite />
          <View>
            <View style={styles.notificationAmount}>
              <Text style={styles.textNotif}>1</Text>
            </View>
            <Image source={IcNotification} style={styles.notification} />
          </View>
        </ImageBackground>
      </View>
      <ScrollView>
        <Image source={BackgroundHome} style={styles.background} />
        {/* Card Saldo */}
        <View style={styles.cardSaldo}>
          <View style={styles.amountContainer}>
            <View style={styles.leftContainer}>
              <IcWallet />
              <Gap width={8} />
              <Text style={styles.textAmount}>Saldo kamu</Text>
            </View>
            <Number style={styles.amount} number={1500000} />
          </View>
          <Gap height={10} />
          <View style={styles.line} />
          <Gap height={15} />
          <View style={styles.buttonActionContainer}>
            <View style={styles.buttonActionContent}>
              <IcTopUp />
              <Gap height={8} />
              <Text style={styles.text}>Top Up</Text>
            </View>
            <View style={styles.spacing} />
            <View style={styles.buttonActionContent}>
              <IcTransfer />
              <Gap height={8} />
              <Text style={styles.text}>Transfer</Text>
            </View>
          </View>
        </View>
        {/* Main Menu */}
        <View style={styles.cardMainMenu}>
          <View style={styles.row}>
            <TouchableOpacity style={styles.mainMenu}>
              <IcPulsa />
              <Text style={styles.textMain}>Pulsa</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcInternet />
              <Text style={styles.textMain}>Internet</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcPLN />
              <Text style={styles.textMain}>PLN</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcEMoney />
              <Text style={styles.textMain}>E-Money</Text>
            </TouchableOpacity>
          </View>
          <Gap height={23} />
          <View style={styles.row}>
            <TouchableOpacity style={styles.mainMenu}>
              <IcTelkom />
              <Text style={styles.textMain}>Telkom</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcPDAM />
              <Text style={styles.textMain}>PDAM</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcRouter />
              <Text style={styles.textMain}>Internet & Tv Kabel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainMenu}>
              <IcMore />
              <Text style={styles.textMain}>Lainnya</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* News */}
        <View style={styles.newsContainer}>
          <View style={styles.newsHeader}>
            <Text style={styles.heading}>Berita MyIrsyad</Text>
            <TouchableOpacity style={styles.more}>
              <Text style={styles.textMore}>Selengkapnya</Text>
              <Gap width={8} />
              <IcArrowDown />
            </TouchableOpacity>
          </View>
          <ListNews />
          <ListNews />
        </View>
      </ScrollView>
    </View>
  );
};

export default PPOB;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  header: {},
  backgroundHeader: {
    flexDirection: 'row',
    height: normalize(98),
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    zIndex: 1,
  },
  background: {
    width: '100%',
    height: normalize(200),
    marginTop: normalize(-86),
  },
  notification: {
    width: normalize(24),
    height: normalize(24),
  },
  notificationAmount: {
    width: normalize(15),
    height: normalize(15),
    borderRadius: normalize(15),
    backgroundColor: '#E74C3C',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -3,
    right: 0,
    zIndex: 1,
  },
  textNotif: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#FFFFFF',
  },
  cardSaldo: {
    backgroundColor: '#FFFFFF',
    height: normalize(128),
    marginHorizontal: normalize(30),
    marginTop: normalize(-85),
    borderRadius: normalize(10),
    elevation: 2,
    padding: normalize(15),
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textAmount: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
  amount: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
  line: {
    height: 1,
    backgroundColor: '#8B8B8B',
  },
  buttonActionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: normalize(57),
  },
  buttonActionContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
  },
  spacing: {
    width: 1,
    height: normalize(30),
    backgroundColor: '#3D3D3D',
  },
  cardMainMenu: {
    backgroundColor: '#FFFFFF',
    borderRadius: normalize(10),
    marginTop: normalize(24),
    marginBottom: normalize(10),
    marginHorizontal: normalize(15),
    paddingHorizontal: normalize(10),
    paddingVertical: normalize(16),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  mainMenu: {
    width: normalize(55),
    height: normalize(80),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textMain: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
    textAlign: 'center',
  },
  newsContainer: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: normalize(15),
    paddingTop: normalize(16),
  },
  newsHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: normalize(24),
  },
  heading: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: '#3D3D3D',
  },
  more: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textMore: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#4584FF',
  },
});
