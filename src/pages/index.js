import SplashScreen from './SplashScreen';
import IntroSlider from './IntroSlider';
import Login from './Login';
import Register from './Register';
import ForgotPassword from './ForgotPassword';
import VerifikasiOTP from './VerifikasiOTP';
import PPOB from './PPOB';
import Ibadah from './Ibadah';
import History from './History';
import DetailTransaction from './DetailTransaction';
import Profile from './Profile';
import Kesiswaan from './Kesiswaan';
import JadwalPelajaran from './JadwalPelajaran';
import JadwalUjian from './JadwalUjian';
import NilaiUjian from './NilaiUjian';
import Absensi from './Absensi';
import MateriPelajaran from './MateriPelajaran';
import AmalYaumi from './AmalYaumi';
import PrestasiSiswa from './PrestasiSiswa';

export {
  SplashScreen,
  IntroSlider,
  Login,
  Register,
  ForgotPassword,
  VerifikasiOTP,
  PPOB,
  Ibadah,
  History,
  DetailTransaction,
  Profile,
  Kesiswaan,
  JadwalPelajaran,
  NilaiUjian,
  JadwalUjian,
  Absensi,
  MateriPelajaran,
  AmalYaumi,
  PrestasiSiswa,
};
