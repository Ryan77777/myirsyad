import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import normalize from 'react-native-normalize';
import {IcCloseCircle, IcSearch} from '../../assets';
import {Gap, Header, ListData} from '../../components';

const PrestasiSiswa = () => {
  return (
    <View style={styles.page}>
      <Header text="Prestasi Siswa" />
      <Gap height={18} />
      <View style={styles.searchBar}>
        <View style={styles.leftSearch}>
          <IcSearch />
          <Gap width={16} />
          <TextInput
            placeholder="Cari materi pelajaran"
            placeholderTextColor="#8B8B8B"
            style={styles.input}
          />
        </View>
        <IcCloseCircle />
      </View>
      <Gap height={18} />
      <ListData
        title="Juara 1 Olimpiade Matematika Tingkat Nasional"
        date="17 Apr 2021"
        type="prestasi"
      />
      <ListData
        title="Juara 1 Karate Tingkat Provinsi"
        date="17 Apr 2021"
        type="prestasi"
      />
      <ListData
        title="Juara 1 Lomba Catur Tingkat Provinsi"
        date="17 Apr 2021"
        type="prestasi"
      />
      <ListData
        title="Juara 1 Lomba Kimia Tingkat Provinsi"
        date="17 Apr 2021"
        type="prestasi"
      />
    </View>
  );
};

export default PrestasiSiswa;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#F7F6FB',
  },
  searchBar: {
    backgroundColor: '#D7D7D7',
    borderRadius: normalize(20),
    paddingLeft: normalize(16),
    paddingRight: normalize(12),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: normalize(15),
  },
  leftSearch: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    width: normalize(250),
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#000000',
  },
});
