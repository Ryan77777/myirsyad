import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {BottomNavigation} from '../components';
import {
  Absensi,
  DetailTransaction,
  ForgotPassword,
  History,
  Ibadah,
  IntroSlider,
  JadwalPelajaran,
  JadwalUjian,
  Kesiswaan,
  Login,
  NilaiUjian,
  PPOB,
  Profile,
  Register,
  SplashScreen,
  VerifikasiOTP,
  MateriPelajaran,
  AmalYaumi,
  PrestasiSiswa,
} from '../pages';
import EditProfile from '../pages/EditProfile';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigation {...props} />}>
      <Tab.Screen name="PPOB" component={PPOB} />
      <Tab.Screen name="Ibadah" component={Ibadah} />
      <Tab.Screen name="Kesiswaan" component={Kesiswaan} />
      <Tab.Screen name="Riwayat" component={History} />
      <Tab.Screen name="Profil" component={Profile} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="MainApp">
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="IntroSlider"
        component={IntroSlider}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="VerifikasiOTP"
        component={VerifikasiOTP}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DetailTransaction"
        component={DetailTransaction}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="JadwalPelajaran"
        component={JadwalPelajaran}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="JadwalUjian"
        component={JadwalUjian}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="NilaiUjian"
        component={NilaiUjian}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Absensi"
        component={Absensi}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MateriPelajaran"
        component={MateriPelajaran}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AmalYaumi"
        component={AmalYaumi}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="PrestasiSiswa"
        component={PrestasiSiswa}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
