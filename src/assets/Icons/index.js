import IcArrowRight from './IcArrowRight.svg';
import IcArrowRightDark from './IcArrowRightDark.svg';
import IcCheck from './IcCheck.svg';
import IcLock from './IcLock.svg';
import IcShow from './IcShow.png';
import IcHide from './IcHide.png';
import IcPPOBActive from './IcPPOBActive.svg';
import IcPPOB from './IcPPOB.svg';
import IcIbadah from './IcIbadah.svg';
import IcIbadahActive from './IcIbadahActive.svg';
import IcKesiswaan from './IcKesiswaan.svg';
import IcKesiswaanActive from './IcKesiswaanActive.svg';
import IcHistory from './IcHistory.svg';
import IcUser from './IcUser.svg';
import IcNotification from './IcNotification.png';
import IcWallet from './IcWallet.svg';
import IcTopUp from './IcTopUp.svg';
import IcScan from './IcScan.svg';
import IcTransfer from './IcTransfer.svg';
import IcPulsa from './IcPulsa.svg';
import IcInternet from './IcInternet.svg';
import IcPLN from './IcPLN.svg';
import IcEMoney from './IcEMoney.svg';
import IcTelkom from './IcTelkom.svg';
import IcPDAM from './IcPDAM.svg';
import IcRouter from './IcRouter.svg';
import IcMore from './IcMore.svg';
import IcArrowDown from './IcArrowDown.svg';
import IcArrowDownDark from './IcArrowDownDark.svg';
import IcArrowLeft from './IcArrowLeft.svg';
import IcBadge from './IcBadge.svg';
import IcEdit from './IcEdit.svg';
import IcLockBlack from './IcLockBlack.svg';
import IcDoc from './IcDoc.svg';
import IcLogout from './IcLogout.svg';
import IcHistoryActive from './IcHistoryActive.svg';
import IcProfileActive from './IcProfileActive.svg';
import IcCalendar from './IcCalendar.png';
import IcArrowUpDark from './IcArrowUpDark.svg';
import IcTick from './IcTick.svg';
import IcInfo from './IcInfo.svg';
import IcClose from './IcClose.svg';
import IcPlus from './IcPlus.svg';
import IcSearch from './IcSearch.svg';
import IcCloseCircle from './IcCloseCircle.svg';
import IcStar from './IcStar.svg';
import IcStarActive from './IcStarActive.svg';
import IcTime from './IcTime.svg';
import IcTimeSquare from './IcTimeSquare.svg';
import IcPrestasi from './IcPrestasi.svg';

export {
  IcArrowRight,
  IcArrowRightDark,
  IcCheck,
  IcLock,
  IcHide,
  IcShow,
  IcHistory,
  IcIbadah,
  IcIbadahActive,
  IcKesiswaan,
  IcPPOBActive,
  IcPPOB,
  IcUser,
  IcNotification,
  IcWallet,
  IcTopUp,
  IcScan,
  IcTransfer,
  IcPLN,
  IcEMoney,
  IcInternet,
  IcPulsa,
  IcMore,
  IcPDAM,
  IcRouter,
  IcTelkom,
  IcArrowDown,
  IcArrowLeft,
  IcBadge,
  IcDoc,
  IcEdit,
  IcLockBlack,
  IcLogout,
  IcHistoryActive,
  IcProfileActive,
  IcKesiswaanActive,
  IcCalendar,
  IcArrowUpDark,
  IcArrowDownDark,
  IcClose,
  IcInfo,
  IcTick,
  IcPlus,
  IcCloseCircle,
  IcSearch,
  IcStarActive,
  IcStar,
  IcTime,
  IcTimeSquare,
  IcPrestasi,
};
