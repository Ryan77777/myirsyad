export default [
  {
    id: '1',
    title: 'Selamat datang di my irsyad',
    description: 'Solusi pintar untuk berbagai pembayaran',
    image: require('../Illustrations/ILIntroSlider.png'),
    skip: false,
  },
  {
    id: '2',
    title: 'Bayar apapun jadi mudah',
    description: 'Bayar apapun jadi mudah hanya dalam satu aplikasi pintar',
    image: require('../Illustrations/ILIntroSlider.png'),
    skip: true,
  },
  {
    id: '3',
    title: 'Keamanan super canggih',
    description:
      'sistem keamanan menjaga uang sekaligus datamu, degan kode pin agar selalu aman',
    image: require('../Illustrations/ILIntroSlider.png'),
    skip: true,
  },
];
