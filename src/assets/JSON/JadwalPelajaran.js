export default [
  {
    day: 'Senin',
    subjects: [
      {
        bg: '#FF8383',
        subject: 'Matematika',
        class: 'XII',
        time: '07.00-09.00',
      },
      {
        bg: '#CD84F1',
        subject: 'Bhs. Inggris',
        class: 'XII',
        time: '09.00-11.00',
      },
      {
        bg: '#FF9F1A',
        subject: 'Kimia',
        class: 'XII',
        time: '13.00-14.00',
      },
      {
        bg: '#F97F51',
        subject: 'Fisika',
        class: 'XII',
        time: '14.00-15.00',
      },
    ],
  },
  {
    day: 'Selasa',
    subjects: [
      {
        bg: '#FF8383',
        subject: 'Matematika',
        class: 'XII',
        time: '07.00-09.00',
      },
      {
        bg: '#CD84F1',
        subject: 'Bhs. Inggris',
        class: 'XII',
        time: '09.00-11.00',
      },
      {
        bg: '#FF9F1A',
        subject: 'Kimia',
        class: 'XII',
        time: '13.00-14.00',
      },
      {
        bg: '#F97F51',
        subject: 'Fisika',
        class: 'XII',
        time: '14.00-15.00',
      },
    ],
  },
  {
    day: 'Rabu',
    subjects: [
      {
        bg: '#FF8383',
        subject: 'Matematika',
        class: 'XII',
        time: '07.00-09.00',
      },
      {
        bg: '#CD84F1',
        subject: 'Bhs. Inggris',
        class: 'XII',
        time: '09.00-11.00',
      },
      {
        bg: '#FF9F1A',
        subject: 'Kimia',
        class: 'XII',
        time: '13.00-14.00',
      },
      {
        bg: '#F97F51',
        subject: 'Fisika',
        class: 'XII',
        time: '14.00-15.00',
      },
    ],
  },
  {
    day: 'Kamis',
    subjects: [
      {
        bg: '#FF8383',
        subject: 'Matematika',
        class: 'XII',
        time: '07.00-09.00',
      },
      {
        bg: '#CD84F1',
        subject: 'Bhs. Inggris',
        class: 'XII',
        time: '09.00-11.00',
      },
      {
        bg: '#FF9F1A',
        subject: 'Kimia',
        class: 'XII',
        time: '13.00-14.00',
      },
      {
        bg: '#F97F51',
        subject: 'Fisika',
        class: 'XII',
        time: '14.00-15.00',
      },
    ],
  },
  {
    day: 'Jumat',
    subjects: [
      {
        bg: '#FF8383',
        subject: 'Matematika',
        class: 'XII',
        time: '07.00-09.00',
      },
      {
        bg: '#CD84F1',
        subject: 'Bhs. Inggris',
        class: 'XII',
        time: '09.00-11.00',
      },
      {
        bg: '#FF9F1A',
        subject: 'Kimia',
        class: 'XII',
        time: '13.00-14.00',
      },
      {
        bg: '#F97F51',
        subject: 'Fisika',
        class: 'XII',
        time: '14.00-15.00',
      },
    ],
  },
  {
    day: 'Sabtu',
    subjects: [
      {
        bg: '#FF8383',
        subject: 'Matematika',
        class: 'XII',
        time: '07.00-09.00',
      },
      {
        bg: '#CD84F1',
        subject: 'Bhs. Inggris',
        class: 'XII',
        time: '09.00-11.00',
      },
      {
        bg: '#FF9F1A',
        subject: 'Kimia',
        class: 'XII',
        time: '13.00-14.00',
      },
      {
        bg: '#F97F51',
        subject: 'Fisika',
        class: 'XII',
        time: '14.00-15.00',
      },
    ],
  },
];
