import Indonesia from './Indonesia.svg';
import Logo from './Logo.svg';
import LogoDark from './LogoDark.svg';
import LogoWhite from './LogoWhite.svg';
import Background from './Background.svg';
import BackgroundHeader from './BackgroundHeader.png';
import BackgroundHome from './BackgroundHome.png';
import BackgroundDetail from './BackgroundDetail.png';
import BackgroundProfile from './BackgroundProfile.png';
import News1 from './News1.jpg';
import News2 from './News2.jpg';
import Header from './Header.png';
import XL from './XL.png';
import Dot from './Dot.svg';
import DotDark from './DotDark.svg';
import User from './User.png';
import Video from './Video.png';
import Play from './Play.svg';
import PDF from './PDF.svg';
import Bookmark from './Bookmark.svg';

export {
  Indonesia,
  Logo,
  Background,
  LogoDark,
  BackgroundHome,
  LogoWhite,
  News1,
  News2,
  BackgroundHeader,
  Header,
  XL,
  Dot,
  BackgroundDetail,
  BackgroundProfile,
  User,
  DotDark,
  Video,
  Play,
  PDF,
  Bookmark,
};
