import React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import normalize from 'react-native-normalize';

const Input = ({placeholder, value}) => {
  return (
    <TextInput
      placeholder={placeholder}
      placeholderTextColor="#C4C4C4"
      style={styles.input}
      value={value}
    />
  );
};

export default Input;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  input: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#3D3D3D',
    borderBottomWidth: 1,
    borderBottomColor: '#C4C4C4',
  },
  inputPassword: {
    width: '100%',
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#3D3D3D',
    borderBottomWidth: 1,
    borderBottomColor: '#C4C4C4',
  },
  icon: {
    width: normalize(24),
    height: normalize(24),
    position: 'absolute',
    top: normalize(12),
    right: normalize(20),
  },
});
