import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import normalize from 'react-native-normalize';

const Button = ({text, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1B6007',
    paddingVertical: normalize(14),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: normalize(50),
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#FFFFFF',
  },
});
