import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {DotDark} from '../../../assets';
import {Gap} from '../../atoms';

const ListUjian = ({title, date, time, value}) => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>{title}</Text>
        <Gap height={6} />
        <View style={styles.dateWrapper}>
          <Text style={styles.datetime}>{date}</Text>
          <Gap width={8} />
          <DotDark />
          <Gap width={8} />
          <Text style={styles.datetime}>{time}</Text>
        </View>
      </View>
      {value <= 100 && value >= 90 && (
        <Text style={styles.valueSuccess}>{value}</Text>
      )}
      {value <= 89 && value >= 60 && <Text style={styles.value}>{value}</Text>}
      {value <= 59 && <Text style={styles.valueDanger}>{value}</Text>}
    </View>
  );
};

export default ListUjian;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(25),
    borderRadius: normalize(10),
    marginBottom: normalize(8),
  },
  dateWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#000000',
  },
  datetime: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
  },
  value: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(20),
    color: '#FFAF40',
  },
  valueSuccess: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(20),
    color: '#1B6007',
  },
  valueDanger: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(20),
    color: '#E74C3C',
  },
});
