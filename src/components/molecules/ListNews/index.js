import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {News1} from '../../../assets';
import {Gap} from '../../atoms';

const ListNews = () => {
  return (
    <View style={styles.newsContent}>
      <Image source={News1} />
      <Gap width={16} />
      <View style={styles.newsDetail}>
        <Text style={styles.title}>Kunjungan Kepada Menteri Agama</Text>
        <Gap height={6} />
        <Text style={styles.date}>21 April 2021</Text>
      </View>
    </View>
  );
};

export default ListNews;

const styles = StyleSheet.create({
  newsContent: {
    flexDirection: 'row',
    paddingBottom: normalize(16),
    borderBottomWidth: 1,
    borderBottomColor: '#D7D7D7',
    marginBottom: normalize(16),
  },
  newsDetail: {
    paddingTop: normalize(6),
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
  date: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#8B8B8B',
  },
});
