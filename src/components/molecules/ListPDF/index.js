import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {Bookmark, IcTime, PDF} from '../../../assets';
import {Gap} from '../../atoms';

const ListPDF = ({title, date}) => {
  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <Bookmark />
        <Gap width={15} />
        <View>
          <Text style={styles.title}>{title}</Text>
          <Gap height={9} />
          <View style={styles.dateContainer}>
            <IcTime />
            <Gap width={6} />
            <Text style={styles.date}>{date}</Text>
          </View>
        </View>
      </View>
      <PDF />
    </View>
  );
};

export default ListPDF;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: normalize(16),
    paddingHorizontal: normalize(7),
    marginBottom: normalize(10),
    borderRadius: normalize(10),
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: '#000000',
    width: normalize(170),
  },
  dateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  date: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#3D3D3D',
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
