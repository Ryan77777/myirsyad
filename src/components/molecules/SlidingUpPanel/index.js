import {BlurView} from '@react-native-community/blur';
import React from 'react';
import {Modal, Pressable, StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import Toast, {BaseToast} from 'react-native-toast-message';
import {IcError, IcSuccess} from '../../../assets';
import {Button, Gap, Input} from '../../atoms';

const SlidingUpPanel = ({title, show, closePopup, type}) => {
  const toastConfig = {
    success: ({text1, props, ...rest}) => (
      <BaseToast
        {...rest}
        style={styles.leftSuccess}
        contentContainerStyle={styles.padding}
        text1Style={styles.textAlert}
        text1={text1}
        text2={props.uuid}
      />
    ),

    errorAlert: ({text1, props, ...rest}) => (
      <View style={styles.alertError}>
        <View style={styles.border}>
          <IcError />
        </View>
        <Gap width={8} />
        <Text style={styles.textAlert}>{text1}</Text>
      </View>
    ),
    successAlert: ({text1, props, ...rest}) => (
      <View style={styles.alertSuccess}>
        <IcSuccess />
        <Gap width={8} />
        <Text style={styles.textAlert}>{text1}</Text>
      </View>
    ),
  };

  return (
    <>
      <Toast config={toastConfig} ref={ref => Toast.setRef(ref)} />
      {show && (
        <BlurView blurType="dark" blurAmount={100} style={styles.blur} />
      )}

      <Modal
        animationType="slide"
        transparent={true}
        visible={show}
        onRequestClose={() => {}}>
        <Pressable
          onPress={() => {
            if (!true) {
              return;
            }
            closePopup();
          }}
          style={styles.pressable}
        />

        {type === 'TambahAmal' && (
          <View style={styles.modal}>
            <Text style={styles.title}>{title}</Text>
            <Gap height={32} />
            <View style={styles.content}>
              <View>
                <Input placeholder="Aktivitas" />
              </View>
              <Button text="Tambah Aktivitas" />
            </View>
          </View>
        )}
      </Modal>
    </>
  );
};

export default SlidingUpPanel;

const styles = StyleSheet.create({
  pressable: {flex: 1},
  blur: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  modal: {
    width: '100%',
    height: '54%',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: normalize(30),
    borderTopRightRadius: normalize(30),
    paddingHorizontal: normalize(15),
    paddingTop: normalize(30),
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(18),
    color: '#3D3D3D',
    textAlign: 'center',
  },
  content: {
    flex: 1,
    paddingHorizontal: normalize(24),
    justifyContent: 'space-between',
    marginBottom: normalize(24),
  },
});
