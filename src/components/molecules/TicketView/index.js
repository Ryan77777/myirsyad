import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {Dot, IcBadge, LogoDark} from '../../../assets';
import {Gap} from '../../atoms';

const TicketView = () => {
  const Circle = () => {
    return <View style={styles.circle} />;
  };
  return (
    <View style={styles.container}>
      <View style={styles.logo}>
        <LogoDark />
      </View>
      <Gap height={24} />
      <View style={styles.firstContainer}>
        <View style={styles.dateContainer}>
          <Text style={styles.datetime}>13 Mei 2021</Text>
          <Gap width={5} />
          <Dot />
          <Gap width={5} />
          <Text style={styles.datetime}>08:21</Text>
        </View>
        <Text style={styles.invoice}>INV00113052021</Text>
      </View>
      <View style={styles.secondContainer}>
        <IcBadge />
        <Gap width={8} />
        <Text style={styles.textSuccess}>Transaksi berhasil</Text>
      </View>
      <Text style={styles.product}>Pulsa XL 081999548521</Text>
      <Gap height={18} />
      <View style={styles.border}>
        <Text style={styles.total}>Total Bayar</Text>
        <Text style={styles.total}>Rp 100.000</Text>
      </View>
      <View style={styles.circleContainer}>
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
      </View>
      <View style={styles.circleContainerBottom}>
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
        <Circle />
      </View>
    </View>
  );
};

export default TicketView;

const styles = StyleSheet.create({
  container: {
    height: normalize(282),
    marginHorizontal: normalize(15),
    backgroundColor: '#FFFFFF',
    paddingHorizontal: normalize(16),
    paddingTop: normalize(32),
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  firstContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#D7D7D7',
    paddingBottom: normalize(7),
  },
  dateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  datetime: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#8D8D8D',
  },
  invoice: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#1E272E',
  },
  secondContainer: {
    flexDirection: 'row',
    paddingVertical: normalize(8),
    alignItems: 'center',
  },
  textSuccess: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#3D3D3D',
  },
  product: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#1E272E',
  },
  border: {
    backgroundColor: '#CEEFE9',
    borderRadius: normalize(5),
    paddingVertical: normalize(10),
    paddingHorizontal: normalize(12),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  total: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#1E272E',
  },
  circle: {
    width: normalize(17),
    height: normalize(17),
    borderRadius: normalize(17),
    backgroundColor: '#10AC84',
    marginRight: normalize(9),
  },
  circleContainer: {
    flexDirection: 'row',
    position: 'absolute',
    marginLeft: normalize(7.5),
    marginTop: normalize(-6),
  },
  circleContainerBottom: {
    flexDirection: 'row',
    position: 'absolute',
    marginLeft: normalize(7.5),
    marginTop: normalize(274),
  },
});
