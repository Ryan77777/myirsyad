import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {DotDark, IcClose, IcInfo, IcTick} from '../../../assets';
import {Gap} from '../../atoms';

const ListAbsensi = ({title, date, time}) => {
  return (
    <View style={styles.container}>
      {title === 'Hadir' && <IcTick />}
      {title === 'Izin' && <IcInfo />}
      {title === 'Sakit' && <IcInfo />}
      {title === 'Alpa' && <IcClose />}
      <Gap width={16} />
      <View>
        <Text style={styles.title}>{title}</Text>
        <Gap height={6} />
        <View style={styles.dateWrapper}>
          <Text style={styles.datetime}>{date}</Text>
          <Gap width={8} />
          <DotDark />
          <Gap width={8} />
          <Text style={styles.datetime}>{time}</Text>
        </View>
      </View>
    </View>
  );
};

export default ListAbsensi;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(25),
    borderBottomWidth: 1,
    borderBottomColor: '#D7D7D7',
  },
  dateWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: '#000000',
  },
  datetime: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#000000',
  },
});
