import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {IcStar, IcStarActive, Play, Video} from '../../../assets';
import {Gap} from '../../atoms';

const ListSubject = ({title, subject, semester}) => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>{title}</Text>
        <Gap height={9} />
        <Text style={styles.subject}>{subject}</Text>
        <Gap height={9} />
        <View style={styles.rating}>
          <IcStarActive />
          <IcStarActive />
          <IcStarActive />
          <IcStarActive />
          <IcStar />
        </View>
        <Gap height={9} />
        <Text style={styles.subject}>{semester}</Text>
      </View>
      <ImageBackground source={Video} style={styles.thumbnail}>
        <TouchableOpacity style={styles.playButton}>
          <Play />
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default ListSubject;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: normalize(12),
    paddingLeft: normalize(16),
    paddingRight: normalize(12),
    marginBottom: normalize(16),
    borderRadius: normalize(10),
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#3D3D3D',
    width: normalize(140),
  },
  subject: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#8B8B8B',
  },
  rating: {
    flexDirection: 'row',
  },
  thumbnail: {
    width: normalize(170),
    height: normalize(116),
    justifyContent: 'center',
    alignItems: 'center',
  },
  playButton: {
    width: normalize(41),
    height: normalize(41),
    backgroundColor: 'rgba(48, 48, 48, 0.5)',
    borderRadius: normalize(41 / 2),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
