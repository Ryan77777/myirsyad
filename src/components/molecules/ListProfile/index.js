import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import normalize from 'react-native-normalize';
import {IcArrowRightDark} from '../../../assets';
import {Gap} from '../../atoms';

const ListProfile = ({text, icon, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={onPress}>
      <View style={styles.leftContainer}>
        {icon}
        <Gap width={16} />
        <Text style={styles.text}>{text}</Text>
      </View>
      <IcArrowRightDark />
    </TouchableOpacity>
  );
};

export default ListProfile;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: normalize(24),
    borderBottomWidth: 1,
    borderBottomColor: '#D7D7D7',
    alignItems: 'center',
    marginBottom: normalize(13),
  },
  leftContainer: {
    flexDirection: 'row',
    paddingBottom: normalize(13),
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
});
