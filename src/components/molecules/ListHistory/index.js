import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import normalize from 'react-native-normalize';
import {Dot, XL} from '../../../assets';
import Number from '../Number';
import {Gap} from '../../../components';

const ListHistory = ({provider, date, time, price, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={onPress}>
      <View style={styles.leftContainer}>
        <Image source={XL} style={styles.icon} />
        <Gap width={16} />
        <View style={styles.detailContainer}>
          <Text style={styles.title}>{provider}</Text>
          <Gap height={3} />
          <View style={styles.dateContainer}>
            <Text style={styles.datetime}>{date}</Text>
            <Gap width={5} />
            <Dot />
            <Gap width={5} />
            <Text style={styles.datetime}>{time}</Text>
          </View>
        </View>
      </View>
      <Number number={price} style={styles.price} />
    </TouchableOpacity>
  );
};

export default ListHistory;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: normalize(16),
    alignItems: 'center',
    borderBottomColor: '#D7D7D7',
    borderBottomWidth: 1,
    marginBottom: normalize(16),
  },
  leftContainer: {
    flexDirection: 'row',
  },
  icon: {
    width: normalize(42),
    height: normalize(42),
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#3D3D3D',
  },
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  datetime: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#8D8D8D',
  },
  detailContainer: {
    justifyContent: 'center',
  },
  price: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(14),
    color: '#3D3D3D',
  },
});
