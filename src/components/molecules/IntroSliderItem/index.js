import React from 'react';
import {Image, StyleSheet, Text, useWindowDimensions, View} from 'react-native';
import normalize from 'react-native-normalize';
import {Gap} from '../../atoms';

const IntroSliderItem = ({item}) => {
  const {width} = useWindowDimensions();

  return (
    <View>
      <View style={[styles.container, {width}]}>
        {item.skip && <Text style={styles.skip}>Skip</Text>}
        <Gap height={140} />
        <Image source={item.image} style={styles.image} />
        <Gap height={40} />
        <View style={styles.content}>
          <Text style={styles.title}>{item.title}</Text>
          <Gap height={8} />
          <Text style={styles.description}>{item.description}</Text>
        </View>
      </View>
    </View>
  );
};

export default IntroSliderItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  skip: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: '#1B6007',
    position: 'absolute',
    top: normalize(24),
    right: normalize(24),
  },
  image: {
    width: normalize(271.48),
    height: normalize(289.02),
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: normalize(64),
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(16),
    color: '#1E272E',
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#1E272E',
    width: normalize(175),
    textAlign: 'center',
  },
  pagination: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
