import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  useWindowDimensions,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import {ListSubject} from '..';
import {IcCloseCircle, IcSearch} from '../../../assets';
import {Gap, ListPDF} from '../../../components';

const renderTabBar = props => {
  return (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicatorStyle}
      style={styles.background}
      tabStyle={styles.tabStyle}
      labelStyle={styles.labelStyle}
      renderLabel={({route, focused}) => (
        <Text style={styles.text(focused)}>{route.title}</Text>
      )}
    />
  );
};

const Video = () => {
  return (
    <View style={styles.container}>
      <View style={styles.searchBar}>
        <View style={styles.leftSearch}>
          <IcSearch />
          <Gap width={16} />
          <TextInput
            placeholder="Cari materi pelajaran"
            placeholderTextColor="#8B8B8B"
            style={styles.input}
          />
        </View>
        <IcCloseCircle />
      </View>
      <Gap height={10} />
      <ListSubject
        title="Besaran Fisika dan pengukurnya"
        subject="Fisika"
        semester="Kelas 10 Semester 1"
      />
      <ListSubject
        title="Besaran Fisika dan pengukurnya"
        subject="Fisika"
        semester="Kelas 10 Semester 1"
      />
      <ListSubject
        title="Besaran Fisika dan pengukurnya"
        subject="Fisika"
        semester="Kelas 10 Semester 1"
      />
    </View>
  );
};

const PDF = () => {
  return (
    <View style={styles.container}>
      <View style={styles.searchBar}>
        <View style={styles.leftSearch}>
          <IcSearch />
          <Gap width={16} />
          <TextInput
            placeholder="Cari materi pelajaran"
            placeholderTextColor="#8B8B8B"
            style={styles.input}
          />
        </View>
        <IcCloseCircle />
      </View>
      <Gap height={24} />
      <ListPDF title="Besaran fisika" date="17 Apr 2021" />
      <ListPDF title="Rumus phitagoras & penerapanya" date="17 Apr 2021" />
    </View>
  );
};

const SubjectTabs = () => {
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '1', title: 'Video'},
    {key: '2', title: 'PDF'},
  ]);

  const renderScene = SceneMap({
    1: Video,
    2: PDF,
  });
  return (
    <View style={styles.tabContainer}>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        style={styles.backgroundColor}
        lazy
      />
    </View>
  );
};

export default SubjectTabs;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: normalize(15),
    paddingTop: normalize(24),
  },
  tabContainer: {
    flex: 1,
  },
  indicatorStyle: {
    height: 3,
    backgroundColor: '#1B6007',
  },
  background: {
    backgroundColor: '#FFFFFF',
    elevation: 0,
  },
  tabStyle: {
    width: normalize(189.5),
  },
  text: focused => ({
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: focused ? '#3D3D3D' : '#8B8B8B',
    width: normalize(189.5),
    textAlign: 'center',
  }),
  backgroundColor: {
    backgroundColor: '#F7F6FB',
  },
  searchBar: {
    backgroundColor: '#D7D7D7',
    borderRadius: normalize(20),
    paddingLeft: normalize(16),
    paddingRight: normalize(12),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  leftSearch: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    width: normalize(250),
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#000000',
  },
});
