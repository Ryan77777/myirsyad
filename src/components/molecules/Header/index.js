import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import normalize from 'react-native-normalize';
import {BackgroundHeader, IcArrowLeft} from '../../../assets';
import {Gap} from '../../atoms';
import {useNavigation} from '@react-navigation/native';

const Header = ({text}) => {
  const navigation = useNavigation();
  return (
    <ImageBackground source={BackgroundHeader} style={styles.background}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          navigation.goBack();
        }}>
        <IcArrowLeft />
      </TouchableOpacity>
      <Gap width={16} />
      <Text style={styles.text}>{text}</Text>
    </ImageBackground>
  );
};

export default Header;

const styles = StyleSheet.create({
  background: {
    height: normalize(84),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: normalize(22),
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: normalize(18),
    color: '#FFFFFF',
  },
});
