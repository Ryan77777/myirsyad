import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import normalize from 'react-native-normalize';
import {
  IcHistory,
  IcHistoryActive,
  IcIbadah,
  IcIbadahActive,
  IcKesiswaan,
  IcKesiswaanActive,
  IcPPOB,
  IcPPOBActive,
  IcProfileActive,
  IcUser,
} from '../../../assets';

const Icon = ({label, active}) => {
  switch (label) {
    case 'PPOB':
      return active ? <IcPPOBActive /> : <IcPPOB />;
    case 'Ibadah':
      return active ? <IcIbadahActive /> : <IcIbadah />;
    case 'Kesiswaan':
      return active ? <IcKesiswaanActive /> : <IcKesiswaan />;
    case 'Riwayat':
      return active ? <IcHistoryActive /> : <IcHistory />;
    case 'Profil':
      return active ? <IcProfileActive /> : <IcUser />;

    default:
      <IcPPOBActive />;
  }
  return <IcPPOBActive />;
};

const BottomNavigation = ({state, descriptors, navigation}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View>
      <View style={styles.container}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              key={index}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={styles.menu}>
              <Icon label={label} active={isFocused} />
              <Text style={styles.text(isFocused)}>{label}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default BottomNavigation;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    paddingVertical: normalize(15),
    paddingHorizontal: normalize(20),
    justifyContent: 'space-between',
    borderTopLeftRadius: normalize(20),
    borderTopRightRadius: normalize(20),
    elevation: 10,
  },
  text: isFocused => ({
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: isFocused ? '#1B6007' : '#8B8B8B',
    paddingTop: normalize(8),
    lineHeight: normalize(16),
  }),
  menu: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
