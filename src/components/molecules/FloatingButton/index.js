import React from 'react';
import {
  Animated,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import normalize from 'react-native-normalize';
import {IcClose, IcInfo, IcPlus, IcTick} from '../../../assets';
import {Gap} from '../../atoms';

const FloatingButton = ({style}) => {
  const animation = new Animated.Value(0);
  let [open] = React.useState(false);

  const toggleMenu = () => {
    const toValue = open ? 0 : 1;
    Animated.spring(animation, {
      toValue,
      friction: 5,
      useNativeDriver: true,
    }).start();

    open = !open;
  };

  const rotation = {
    transform: [
      {
        rotate: animation.interpolate({
          inputRange: [0, 1],
          outputRange: ['0deg', '45deg'],
        }),
      },
    ],
  };

  const pinStyle = {
    transform: [
      {scale: animation},
      {
        translateY: animation.interpolate({
          inputRange: [0, 1],
          outputRange: [0, -60],
        }),
      },
    ],
  };

  return (
    <View style={[styles.container, style]}>
      <TouchableWithoutFeedback>
        <Animated.View style={[styles.secondary, pinStyle]}>
          <View style={styles.row}>
            <IcTick />
            <Gap width={8} />
            <Text style={styles.text}>Hadir</Text>
          </View>
          <View style={styles.row}>
            <IcInfo />
            <Gap width={8} />
            <Text style={styles.text}>Izin</Text>
          </View>
          <View style={styles.row}>
            <IcInfo />
            <Gap width={8} />
            <Text style={styles.text}>Sakit</Text>
          </View>
          <View style={styles.row}>
            <IcClose />
            <Gap width={8} />
            <Text style={styles.text}>Alpa</Text>
          </View>
        </Animated.View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={toggleMenu}>
        <Animated.View style={[styles.button, styles.menu, rotation]}>
          <IcPlus />
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default FloatingButton;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  button: {
    position: 'absolute',
    width: normalize(60),
    height: normalize(60),
    borderRadius: normalize(60 / 2),
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 5,
  },
  menu: {
    backgroundColor: '#FF9F1A',
  },
  secondary: {
    width: normalize(120),
    height: normalize(183),
    borderBottomRightRadius: normalize(48),
    borderBottomLeftRadius: normalize(10),
    borderTopLeftRadius: normalize(10),
    borderTopRightRadius: normalize(10),
    backgroundColor: '#FFECD2',
    paddingHorizontal: normalize(10),
    paddingTop: normalize(16),
    marginBottom: normalize(-55),
  },
  row: {
    flexDirection: 'row',
    marginBottom: normalize(6),
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(12),
    color: '#000000',
  },
});
