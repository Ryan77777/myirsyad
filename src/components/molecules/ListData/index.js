import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {IcPrestasi, IcTime, IcTimeSquare} from '../../../assets';
import {Gap} from '../../atoms';

const ListData = ({title, date, detail, type}) => {
  return (
    <View style={styles.container}>
      <View style={styles.left}>
        {type === 'prestasi' ? <IcPrestasi /> : <IcTimeSquare />}
        <Gap width={15} />
        <View>
          <Text style={styles.title}>{title}</Text>
          <Gap height={9} />
          <View style={styles.dateContainer}>
            <IcTime />
            <Gap width={6} />
            <Text style={styles.date}>{date}</Text>
            <Gap width={12} />
            <Text style={styles.date}>{detail}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ListData;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: normalize(-14),
    paddingVertical: normalize(14),
    marginBottom: normalize(10),
    borderRadius: normalize(10),
    borderBottomWidth: 1,
    borderBottomColor: '#D7D7D7',
    marginLeft: normalize(21),
    marginRight: normalize(35),
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(16),
    color: '#000000',
  },
  dateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  date: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#3D3D3D',
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
